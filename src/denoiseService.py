#!/usr/bin/env python
# -*- coding: utf-8 -*-
import threading
from ComssServiceDevelopment.connectors.tcp.msg_stream_connector import InputMessageConnector, OutputMessageConnector #import modułów konektora msg_stream_connector
from ComssServiceDevelopment.connectors.tcp.object_connector import InputObjectConnector, OutputObjectConnector #import modułów konektora object_connector
from ComssServiceDevelopment.service import Service, ServiceController #import modułów klasy bazowej Service oraz kontrolera usługi
import cv2 #import modułu biblioteki OpenCV
import numpy as np #import modułu biblioteki Numpy
import scipy 
from scipy import ndimage
import subprocess
import sys
import cv2.cv as cv

class DenoiseService(Service): #klasa usługi musi dziedziczyć po ComssServiceDevelopment.service.Service
    def __init__(self): #"nie"konstruktor, inicjalizator obiektu usługi
        super(DenoiseService, self).__init__() #wywołanie metody inicjalizatora klasy nadrzędnej
#        self.filters = [] #zmienna do przechowywania wartości parametru
#        self.filters_lock = threading.RLock() #obiekt pozwalający na blokadę wątku

    def declare_outputs(self): #deklaracja wyjść
        self.declare_output("videoOutput", OutputMessageConnector(self)) #deklaracja wyjścia "videoOutput" będącego interfejsem wyjściowym konektora msg_stream_connector
        self.declare_output("videoOutputForDetector", OutputMessageConnector(self)) #deklaracja wyjścia "videoOutput" będącego interfejsem wyjściowym konektora msg_stream_connector
        self.declare_output("filtersOnOutput", OutputObjectConnector(self)) #deklaracja wyjścia "filtersOnOutput" będącego interfejsem wyjściowym konektora object_connector

    def declare_inputs(self):
        self.declare_input("videoInput", InputMessageConnector(self)) #deklaracja wejścia "videoInput" będącego interfejsem wyjściowym konektora msg_stream_connector
        self.declare_input("filtersOnInput", InputObjectConnector(self)) #deklaracja wejścia "filtersOnInput" będącego interfejsem wyjściowym konektora object_connector

#    def watch_filters(self): #metoda obsługująca strumień sterujacy parametrem usługi
#        filter_input = self.get_input("filtersOnInput") #obiekt interfejsu wejściowego
#        filter_output = self.get_output("filtersOnOutput") #obiekt interfejsu wyjściowego
#        while self.running(): #główna pętla wątku obsługującego strumień sterujący
#            new_filters = filter_input.read() #odczyt danych z interfejsu wejściowego
#            with self.filters_lock:  #blokada wątku
#                self.filters = new_filters #ustawienie wartości parametru
#            filter_output.send(new_filters) #przesłanie danych za pomocą interfejsu wyjściowego 

    def run(self): #główna metoda usługi
        #threading.Thread(target=self.watch_filters).start() #uruchomienie wątku obsługującego strumień sterujący

        videoInput = self.get_input("videoInput") #obiekt interfejsu wejściowego
        videoOutput = self.get_output("videoOutput") #obiekt interfejsu wyjściowego
        videoOutputForDetector = self.get_output("videoOutputForDetector") #obiekt interfejsu wyjściowego
        #ret = subprocess.Popen('python /home/norrec/Programming/python/tirt/src/input.py', shell=True)

        while self.running(): #pętla główna usługi (wątku głównego obsługującego strumień wideo)
            frame_obj = videoInput.read() #odebranie danych z interfejsu wejściowego
            outFrame = np.loads(frame_obj) #załadowanie ramki do obiektu NumPy

            gaussianOutput = cv2.GaussianBlur(outFrame,(9,9),0)

            videoOutput.send(gaussianOutput.dumps()) #przesłanie ramki za pomocą interfejsu wyjściowego
            videoOutputForDetector.send(gaussianOutput.dumps()) #przesłanie ramki za pomocą interfejsu wyjściowego

if __name__=="__main__":
    print "Starting noiseService.py"
    try:
        sc = ServiceController(DenoiseService, "denoiseservice.json") #utworzenie obiektu kontrolera usługi
        sc.start() #uruchomienie usługi
    except Exception:
        sys.exit()
