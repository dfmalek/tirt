#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Tkinter as tk
import threading
import subprocess
import sys
import time

from ComssServiceDevelopment.connectors.tcp.msg_stream_connector import InputMessageConnector #import modułów konektora msg_stream_connector
from ComssServiceDevelopment.connectors.tcp.object_connector import InputObjectConnector
from ComssServiceDevelopment.development import DevServiceController #import modułu klasy testowego kontrolera usługi
import cv2 #import modułu biblioteki OpenCV
import numpy as np #import modułu biblioteki Numpy
import PIL.Image
import PIL.ImageTk

print "Starting output.py"
service_controller = DevServiceController("ballspointerservice.json")
service_controller.declare_connection("videoOutput", InputMessageConnector(service_controller))

def update_image(imageLabel, connection, primitive):
    obj = connection.read() #read data from input interface
    frame = np.loads(obj) #loads data to numPyi
    drawPrimitive(primitive, frame)
    #cv2.circle(frame, (200,200),100,255,-1)
    radius = 50
    print primitive.get()
    if primitive.get() == 1:
        
        cv2.rectangle(frame, (200-radius,200-radius), (200+radius,200+radius), (100,100,100))
    elif primitive.get() == 2:
        cv2.circle(frame, (200,200),100,255,-1)
    elif primitive.get() == 3:
        cv2.ellipse(frame, (200,200), (37,21), -108, 0 , 360, (255,0,0), 2)
    elif primitive.get() == 4:
        print type(frame)
    # cv2.ellipse(frame, (200,200), cv2.Size( 150.0, 50.0), 135, 0, 360,)
               
    frame = cv2.cvtColor(frame,cv2.COLOR_RGB2BGR);
    pil_image = PIL.Image.fromarray(frame)
    tk_image = PIL.ImageTk.PhotoImage(image=pil_image)
    imageLabel.imgtk = tk_image
    imageLabel.configure(image=tk_image)
    imageLabel._image_cache = tk_image  # avoid garbage collection
    root.update()

def drawPrimitive(variable, frame):
    if variable == 1:
        print variable
        #rectangle(frame, Point( 15, 20  ), Point( 70, 50 ), Scalar( 0, 55, 255  ), +1, 4  );
    elif variable == 2:
        print variable
        #cv2.circle(frame, (200,200),100,255,-1)
    elif variable == 3:
        print variable
        #ellipse(frame, Point( 200, 200  ), Size( 150.0, 50.0  ), 135, 0, 360, Scalar( 0, 255, 0  ), 1, 8  );

def update_all(root, imageLabel, connection, primitive):
    if root.quit_flag:
        root.destroy()  # this avoids the update event being in limbo
    else:
        update_image(imageLabel, connection, primitive)
        root.after(1, func=lambda: update_all(root, imageLabel, connection, primitive))

def close_window(): 
    root.quit()

if __name__ == '__main__':
    connection = service_controller.get_connection("videoOutput")

    root = tk.Tk()
    root.bind('<Escape>', lambda e: root.quit())
    panes = tk.PanedWindow(root)
    panes.pack(fill="both", expand="yes")

    controlLabel = tk.Label(panes, text="Control Label Under Development")
    controlLabel.pack()

    setattr(root, 'quit_flag', False)
    def set_quit_flag():
        root.quit_flag = True
    root.protocol('WM_DELETE_WINDOW', set_quit_flag)  # avoid errors on exit
    imageLabel = tk.Label(root)  # video Label
    imageLabel.pack()

    v = tk.IntVar()
    v.set(1)
    exitButton = tk.Button(controlLabel, text = "Exit", command = close_window)
    tk.Radiobutton(controlLabel, text="Square", variable=v, value=1).pack(anchor="n")
    tk.Radiobutton(controlLabel, text="Circle", variable=v, value=2).pack(anchor="e")
    tk.Radiobutton(controlLabel, text="Ellipse", variable=v, value=3).pack(anchor="s")
    tk.Radiobutton(controlLabel, text="Panda", variable=v, value=4).pack(anchor="s")
    
    exitButton.pack()

    panes.add(controlLabel)
    panes.add(imageLabel)
    update_all(root, imageLabel, connection, v)
    root.after(1, func=lambda: update_all(root, imageLabel, connection, v))
    root.mainloop()
