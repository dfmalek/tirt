#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import signal
import subprocess
import time
import sys

print "Starting controller.py"

outputProcess = subprocess.Popen('exec python output.py', shell=True, preexec_fn=os.setsid)
print 'outputProcess PID: ', outputProcess.pid
time.sleep(1.5)
ballsPointerServiceProcess = subprocess.Popen('exec python ballsPointerService.py', shell=True, preexec_fn=os.setsid)
print 'ballsPointerServiceProcess PID: ', ballsPointerServiceProcess.pid
time.sleep(1.5)
ballsDetectorServiceProcess = subprocess.Popen('exec python ballsDetectorService.py', shell=True, preexec_fn=os.setsid)
print 'ballsDetectorServiceProcess PID: ', ballsDetectorServiceProcess.pid
time.sleep(1.5)
denoiseServiceProcess = subprocess.Popen('exec python denoiseService.py', shell=True, preexec_fn=os.setsid)
print 'denoiseServiceProcess PID: ', denoiseServiceProcess.pid
time.sleep(1.5)
inputProcess = subprocess.Popen('exec python input.py', shell=True, preexec_fn=os.setsid)
time.sleep(1)
exitResponse = raw_input("Press any key to exit...")
os.killpg(inputProcess.pid, signal.SIGTERM)
time.sleep(0.5)
os.killpg(denoiseServiceProcess.pid, signal.SIGTERM)
time.sleep(0.5)
os.killpg(ballsDetectorServiceProcess.pid, signal.SIGTERM)
time.sleep(0.5)
os.killpg(ballsPointerServiceProcess.pid, signal.SIGTERM)
time.sleep(0.5)
os.killpg(outputProcess.pid, signal.SIGTERM)
sys.exit()
