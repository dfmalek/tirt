#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ComssServiceDevelopment.connectors.tcp.msg_stream_connector import OutputMessageConnector #import modułów konektora msg_stream_connector
from ComssServiceDevelopment.connectors.tcp.object_connector import OutputObjectConnector #import modułów konektora object_connector
from ComssServiceDevelopment.development import DevServiceController #import modułu klasy testowego kontrolera usługi

import cv2 #import modułu biblioteki OpenCV
import Tkinter as tk #import modułu biblioteki Tkinter -- okienka

print "Starting input.py"
service_controller = DevServiceController("denoiseservice.json") #utworzenie obiektu kontroletra testowego, jako parametr podany jest plik konfiguracji usługi, do której "zaślepka" jest dołączana
service_controller.declare_connection("videoInput", OutputMessageConnector(service_controller)) #deklaracja interfejsu wyjściowego konektora msg_stream_connector, należy zwrócić uwagę, iż identyfikator musi być zgodny z WEJŚCIEM usługi, do której "zaślepka" jest podłączana

#root = tk.Tk()
#root.title("Filters")  #utworzenie okienka

cam = cv2.VideoCapture(0) #"podłączenie" do strumienia wideo z kamerki

#root.after(0, func=lambda: update_all(root, cam, set())) #dołączenie metody update_all do głównej pętli programu, wynika ze specyfiki TKinter
#root.mainloop()

#def update_all(root, cam, filters):
while True:
    read_successful, frame = cam.read() #odczyt obrazu z kamery
    #new_filters = set()

    #if filters ^ new_filters:
    #    filters.clear()
    #    filters.update(new_filters)
    #    service_controller.get_connection("filtersOnInput").send(list(filters)) #wysłanie wartości parametru streującego w zależności od checkbox'a

    frame_dump = frame.dumps() #zrzut ramki wideo do postaci ciągu bajtów
    service_controller.get_connection("videoInput").send(frame_dump) #wysłanie danych ramki wideo
    #root.update()
    #root.after(20, func=lambda: update_all(root, cam, filters))
