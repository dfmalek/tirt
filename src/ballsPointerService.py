#!/usr/bin/env python
# -*- coding: utf-8 -*-
import threading
from ComssServiceDevelopment.connectors.tcp.msg_stream_connector import InputMessageConnector, OutputMessageConnector #import modułów konektora msg_stream_connector
from ComssServiceDevelopment.connectors.tcp.object_connector import InputObjectConnector, OutputObjectConnector #import modułów konektora object_connector
from ComssServiceDevelopment.service import Service, ServiceController #import modułów klasy bazowej Service oraz kontrolera usługi
import cv2 #import modułu biblioteki OpenCV
import numpy as np #import modułu biblioteki Numpy
import scipy 
from scipy import ndimage
import subprocess
import sys

class BallsPointerService(Service): #klasa usługi musi dziedziczyć po ComssServiceDevelopment.service.Service
    def __init__(self): #"nie"konstruktor, inicjalizator obiektu usługi
        super(BallsPointerService, self).__init__() #wywołanie metody inicjalizatora klasy nadrzędnej

    def declare_outputs(self): #deklaracja wyjść
        self.declare_output("videoOutput", OutputMessageConnector(self)) #deklaracja wyjścia "videoOutput" będącego interfejsem wyjściowym konektora msg_stream_connector

    def declare_inputs(self):
        self.declare_input("videoInput", InputMessageConnector(self)) #deklaracja wejścia "videoInput" będącego interfejsem wyjściowym konektora msg_stream_connector
        self.declare_input("ballsPositionsInput", InputMessageConnector(self))

    def run(self): #główna metoda usługi
        videoInput = self.get_input("videoInput") #obiekt interfejsu wejściowego
        videoOutput = self.get_output("videoOutput") #obiekt interfejsu wyjściowego
        ballsPositionsInput = self.get_input("ballsPositionsInput") #obiekt interfejsu wejściowego

        while self.running(): #pętla główna usługi (wątku głównego obsługującego strumień wideo)
            frame_obj = videoInput.read() #odebranie danych z interfejsu wejściowego
            outFrame = np.loads(frame_obj) #załadowanie ramki do obiektu NumPy

            circles = np.frombuffer(ballsPositionsInput.read())
            circles = circles.astype(int)
            # print circles
            # print ""

            cv2.circle(outFrame,(circles[0],circles[1]),circles[2],(0,255,0),2)

            videoOutput.send(outFrame.dumps()) #przesłanie ramki za pomocą interfejsu wyjściowego


if __name__=="__main__":
    print "Starting ballsPointerService.py"
    # try:
    sc = ServiceController(BallsPointerService, "ballspointerservice.json") #utworzenie obiektu kontrolera usługi
    sc.start() #uruchomienie usługi
    # except Exception:
    #     sys.exit()
