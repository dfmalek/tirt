#!/usr/bin/env python
# -*- coding: utf-8 -*-
import threading
from ComssServiceDevelopment.connectors.tcp.msg_stream_connector import InputMessageConnector, OutputMessageConnector #import modułów konektora msg_stream_connector
from ComssServiceDevelopment.connectors.tcp.object_connector import InputObjectConnector, OutputObjectConnector #import modułów konektora object_connector
from ComssServiceDevelopment.service import Service, ServiceController #import modułów klasy bazowej Service oraz kontrolera usługi
import cv2 #import modułu biblioteki OpenCV
import numpy as np #import modułu biblioteki Numpy
import scipy 
from scipy import ndimage
import subprocess
import sys
import cv2.cv as cv

class BallsDetectorService(Service): #klasa usługi musi dziedziczyć po ComssServiceDevelopment.service.Service
    def __init__(self): #"nie"konstruktor, inicjalizator obiektu usługi
        super(BallsDetectorService, self).__init__() #wywołanie metody inicjalizatora klasy nadrzędnej

    def declare_outputs(self): #deklaracja wyjść
        self.declare_output("ballsPositionsOutput", OutputMessageConnector(self)) #deklaracja wyjścia "videoOutput" będącego interfejsem wyjściowym konektora msg_stream_connector

    def declare_inputs(self):
        self.declare_input("videoInput", InputMessageConnector(self)) #deklaracja wejścia "videoInput" będącego interfejsem wyjściowym konektora msg_stream_connector

    def run(self): #główna metoda usługi
        videoInput = self.get_input("videoInput") #obiekt interfejsu wejściowego
        ballsPositionsOutput = self.get_output("ballsPositionsOutput") #obiekt interfejsu wyjściowego
        hsv_min = np.array([70,5,50])	#20,40,20
        hsv_max = np.array([100,100,150]) #100,255,255

        while self.running(): #pętla główna usługi (wątku głównego obsługującego strumień wideo)

            frame_obj = videoInput.read() #odebranie danych z interfejsu wejściowego
            outFrame = np.loads(frame_obj)

            #Progowanie po barwie
            mHSV = cv2.cvtColor(outFrame,cv2.COLOR_RGB2HSV)
            mProgKoloru = cv2.inRange(mHSV,hsv_min,hsv_max)

            #Wykrywanie okregow
            circles = cv2.HoughCircles(mProgKoloru,cv.CV_HOUGH_GRADIENT,4,20,param1=50,param2=30,minRadius=20,maxRadius=0)
            if circles is not None:
                positions = np.array([circles[0][0][0].item(),circles[0][0][1].item(),circles[0][0][2].item()])
                positions.astype(int)
                ballsPositionsOutput.send(np.getbuffer(positions)) #przesłanie ramki za pomocą interfejsu wyjściowego
            else:
                ballsPositionsOutput.send(np.getbuffer(np.array([0,0,0]).astype(int))) #przesłanie ramki za pomocą interfejsu wyjściowego


if __name__=="__main__":
    print "Starting ballsDetectorService.py"
    # try:
    sc = ServiceController(BallsDetectorService, "ballsdetectorservice.json") #utworzenie obiektu kontrolera usługi
    sc.start() #uruchomienie usługi
    # except Exception:
    #     sys.exit()
